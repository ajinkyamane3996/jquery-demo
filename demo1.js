// var VS let
//var scope is globle scope
//let scope is block scope
let name='Adity';

if (true) {
    let name='ajinkya';
    console.log(name);
    
  if (true)  {
    let name='Sumit';
    console.log(name);
   }
}

if (true) {
   console.log(name);
}

let x = 'Ajinkya';
let y = 'Mane';
//let z=x.concat(y);
console.log(x.concat(" ",y));

// Array start ==============================


const cars=['BMW','AUDI','Tesala','Porache'];
let car1=cars[0];
let car2=cars[1];

console.log("First car : "+car1+" & "+" Second car : "+ car2);
console.log(cars);

const numbers=['one','two','three','four'];

console.log("Total numbers :"+numbers);
console.log("Length : "+numbers.length);

console.log(numbers.shift());      //Remove Element at index[0]     
console.log("After shift() : "+numbers);

console.log(numbers.unshift('six'));   //Add Element at index[0]
console.log("After unshift() : "+numbers);

console.log("element to be Deleted :"+numbers.pop()); //pop()-remove last element
console.log(numbers);

console.log(numbers.push('five')); //push()-Add  element at last
console.log(numbers);

numbers.splice(2,1,'one')
console.log(numbers);


// ======== for loop ==========
const days=['Mon','Tue','Wed','Thr','Fri','Sat','Sun'];
console.log(days);
console.log("Length : "+days.length);

for (let i = 0; i < days.length; i++) {
  // const element = days[i];
   console.log(days[i]); 
} 
 console.log("============");
for (let i = days.length -1; i >= 0; i--) {
  // const element = array[i];
  console.log(days[i]);
}

// ======== for each loop ==========
console.log("============");
days.forEach(function(day,index){
   console.log(`Starts with ${index} --- ${day}`);
});
    //**OR**
console.log("============");
days.forEach(element => {
   console.log(element)
}); 

// Grde in college
   function grtmygrade(currentmarks,totalmarks)
   {
      let mypersent = (currentmarks/totalmarks)*100;

      let mygade ='';

      if (mypersent >= 90) {
          mygade='A';
      } else if (mypersent >= 80) {
          mygade='B';       
      } else if (mypersent >= 70) {
          mygade='C';
      } else if (mypersent >= 60) {
          mygade='D';
      } else {
          mygade='F';
      }
   return ` Your grade is ${mygade} and percentage is ${mypersent}.`
}  
let yourresult=grtmygrade(92,100);
console.log(yourresult);

// === Login form validation===
 let userEmail='lco12'
 let password='1234'
 function userChecker(myString){
    if ( (myString.includes(123)) && (myString.length > 6)) {
       return true
    }
    return false
 }
 console.log(userChecker(userEmail));
 function passChecker(password) {
    if ( (password.includes(1234)) && ( password.length > 8)) {
       return true
    }
    return false
 }
 console.log(userChecker(password));
