//ES-6  (let,const)

//Exmple : 1

var count = 100; //var -Global scope
function Increment() {
    let count = 0;  //let -Block scope
    count += 1;
    console.log("In fun count :" + count);
}
Increment();
console.log("Out fun count :" + count);

//Exmple : 2

const x = 10; //var -Global 
//x=20;  //<-we cant reassign value to const declared variable
function show() {
    let x = 50;  //let -Block scope
    x += 1;
    console.log("In fun x :" + x);
}
show();
console.log("Out fun x :" + x);


// Exmple : 1  
//ES-5
function multiplication(num1, num2) {
    let mul = num1 * num2;
    console.log("multipication :" + mul);
}
multiplication(10, 12);
multiplication(15, 5);
//**or
var multiplication = function(num1, num2) {
    return num1 * num2;
}
console.log("multipication :" + multiplication(10, 12));
console.log("multipication :" + multiplication(15, 5));

//ES-6 ( => i.e Arrow  )
multiplication=(num1, num2)=> {
    let mul = num1 * num2;
    console.log("multipication :" + mul);
}
multiplication(10, 12);
multiplication(15, 5);

 var multiplication=function(num1, num2) {
    let mul = num1 * num2;
    console.log("multipication :" + mul);}
console.log("multipication :" + multiplication(10, 12));
console.log("multipication :" + multiplication(15, 5));


function print(no=10,name="ABC") //default parameters 
{
    console.log(no+" "+name);
}
print(1,'Arjun');
print(50);
print();

function add(x, y = 1, z = 2) {
    console.log( arguments.length );
    return x + y + z;
}

add(10); // 1
add(10, 20); // 2
add(10, 20, 30); // 3



