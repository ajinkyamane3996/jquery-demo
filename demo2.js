
const myTodos =['Buy bread','Go to GYM','Record vidio','Walk on road']

console.log(myTodos);
console.log(myTodos.indexOf('Buy bread'))  //Return index of that element
console.log(myTodos[myTodos.indexOf('Buy bread')])   //Return value at perticular index : arr[index]

myTodos.forEach(element => {
    console.log(element);
});
//OR
myTodos.forEach(function(element,index){
        console.log(`Element at index [${index}] = ${element}`)
});
//OR
for (let i = 0 ; i < myTodos.length; ++ i) {
    const element =myTodos[i];
    console.log(element)
}
  
 const newTodos =    //Array
    [{ 
      title:'Buy bread',
      isDone:false,
    },
    {
      title:'Go to GYM',
      isDone:false, 
     },
     {
    title:'Walk on road',
    isDone:false,
    }]
    // console.log(newTodos.length)
    const index= newTodos.findIndex(function(element,index){
        console.log(element)
        console.log(index)
        return element.title ===  'Go to GYM'
    });
    console.log(index);

    //===== Metod 1 ======
    const findTodo1= function (myTodos,title) {
        const index=myTodos.findIndex(function(todo,index){
            return todo.title.toLowerCase() === title.toLowerCase()
        })
        return myTodos[index]   // It return value at -- arr[1] --> { title: 'Go to GYM', isDone: false }
    }
    let printMe1 = findTodo1(newTodos,'Go to GYM')
    console.log(printMe1)

    //===== Metod 2 ======
    const findTodo2=function(myT,title){
        const titleReturn = myT.find(function(todo,index){
            return todo.title.toLowerCase() === title.toLowerCase()
        })
        return titleReturn 
    }
    let printMe2 = findTodo2(newTodos,'Go to GYM')
    console.log(printMe2)
    console.log('=============')

    // ( == ) Vs ( === ) 
    
    // ==   ---> value are equle
    // ===  ---> object which are refferencing in membery is same or not
    //  double equals operator converts the types of the objects being 
    //  compared to the same type Because of this the triple equals operator should almost 
    //  always be used, except in the case of checking if an object is null or undefined
 
   // console.log(2>3)
   // console.log(2<3)
    // console.log(2==2)    
    // console.log(2==2.0) 
    // console.log(2===2.0)

    console.log(2 == '2')  //true
    console.log(2 === '2') //false

    console.log(0==false)  //true
    console.log(0===false) //false

    console.log(null == undefined)  //true
    console.log(null === undefined) //false

    console.log(2 != '2')  //false
    console.log(2 !== '2') //true

    let a='sunbeam' // value 
    let b='sunbeam' 
    console.log(a==b)   //true
    console.log(a===b)  //true

    let x={} //obj1
    let y={} //obj2
    console.log(x==y)   //false
    console.log(x===y) //false


    
