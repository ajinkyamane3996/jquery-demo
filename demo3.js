
// Arrow function => [ES-6] //

//Example-1  General
const sum = function(){
    let a=10;
    let b=20;
    return a+b;
}
console.log("Sum is : "+sum());

let a=10;b=20,c=10;
//const sum1 = function( ){ return a+b }  
const sum1 = () => a+b+c
console.log("Sum is : "+sum1());

//const sum2 = function(a,b){ return a+b }
const sum2 = (a,b,c) => a+b+c
console.log("Sum is : "+sum2(10,25,20));

//const squre=function(a){return a*a}
const squre = (a) => a*a
console.log(`Squre is :` + squre(5))

//Example-2  Array
const persones=[
    {
     fName:'Ajinkya',
     lName:'Mane',
     isDone:'true'
    },{
     fName:'Sumit',
     lName:'Kadam',
     isDone:'false'
    },{
     fName:'Rohan',
     lName:'Sawant',
     isDone:'true'
    }]

//callbackfn <--Read more 

//ES5
persones.forEach(function (value,index){
  console.log( "index["+index+"]" + " = " + value.fName +" "+value.lName )
})
//ES6
//persones.forEach((value,index)=>console.log(index + " = " + value.fName +" "+value.lName))
persones.forEach( (value,index) => console.log( ` index[${index}] & Full Name: ${value.fName} ${value.lName}`))  // In string literal format

  //callbackfn <--Read more 
 // const thisperson = persones.filter(function(person){return  person.isDone === 'true'})
    const thisperson = persones.filter((person)=> person.isDone === 'true')
    console.log(thisperson);
 
  //callbackfn <--Read more 
 // console.log(persones.map(function(person){ return person.fName + " "+ person.lName }))
    console.log(persones.map((person)=> `First name : ${person.fName}  $Last Name : ${person.lName}`))

//Example-3

const materials = [
    'Hydrogen',
    'Helium',
    'Lithium',
    'Beryllium'
]
//console.log( materials.map( function(material){ return material.length }))
//console.log( materials.map( (material)=> material.length ))
// Parentheses are optional when there's only one parameter name:
console.log( materials.map( material=> material.length ))

//Example-3  Object Literal Syntax

//ES5
var setNameIdsEs5 = function (id, name) {
    return { id: id,name: name};};
console.log(setNameIdsEs5(3,'Ajinkya'))
//ES6
var setNameIdsEs6 = (id, name) => ({ id: id, name: name});
console.log(setNameIdsEs6(3,'Ajinkya'))

//Example-3  Array--[Object literals]
/*
 If you are returning an object literal,it needs to be wrapped in parentheses. 
 This forces the interpreter to evaluate what is inside the parentheses, and 
 the object literal is returned.

 syntax  |  x =>({ y: x })     --> here y = propery & x = value
 */

const smartPhones = [   //obj created
    { name:'iphone', price:649 },
    { name:'Galaxy S6', price:576 },
    { name:'Galaxy Note 5', price:489 }
  ];

  //ES5
  var prices1 =smartPhones.map(function(phone){ return   phone.name + " : " + phone.price})
  console.log(prices1);
  // ES6  
  var prices2 =smartPhones.map((phone) => phone.name + " : " + phone.price )
  console.log(prices2);


  let dog = {
    name: "Spot",
    numLegs: 4,
   sayLegs:function (){
      return "This dog has " + dog.numLegs + " legs.";
      } 
  };
  dog.sayLegs();

 /* 
  this is a deep topic, and the above example is only one way to use it.
   In the current context, this refers to the object that the method is 
   associated with: duck. If the object's name is changed to mallard, 
   it is not necessary to find all the references to duck in the code.
    It makes the code reusable and easier to read.        
 */

 /*
  let dog = {
    name: "Spot",
    numLegs: 4,
    sayLegs: function() {return "This dog has " + this.numLegs + " legs.";}
  };
  
  dog.sayLegs();
*/

 function Dog(name,color) {
    this.name = name;
    this.color = color;
    this.numLegs = 4;
  }
  
  let terrier= new Dog('Bruce','red');  // new operator is used when calling a constructor. This tells JavaScript to create a new instance of Bird called blueBird. 
 // This gives a new instance of Dog with name and color properties set to Bruce and red,
 // respectively. The numLegs property is still set to 4. The cardinal has these propertie (name,color,numLegs)
  console.log(terrier.name)   // => Bruce
  console.log(terrier.color)   // => red
  console.log(terrier.numLegs)   // => 4
  let instance= terrier instanceof Dog; // => true [Instanceof allows you to compare an object to a constructor,returning true or false]
  console.log(instance)


  //Map 
  var Ajinkya= {
    fName:'Ajinkya',
    lName:'Mane',
    isDone:'true'
   }
  var Sumit= {
    fName:'Sumit',
    lName:'Kadam',
    isDone:'false'
   }
  var Rohan= {
    fName:'Rohan',
    lName:'Sawant',
    isDone:'true'
   }

  let users=new Map();
  users.set('Ajinkya',Ajinkya)   //set('key',value) <--We add 3 elememnts in users
  users.set('Sumit',Sumit)   
  users.set('Rohan',Rohan)

  console.log(users)
  console.log('Toata users :'+users.size)
  console.log(users.get('Sumit'))      //get(key) : get value by ussing Key [Ajinkya or Sumit or Rohan]
  console.log(users.has('Rohan'))     //return type : Boolean
  console.log(users.keys())     // Returns an iterable of keys in the map .. keys(): IterableIterator<k>;
  console.log(users.values())   // Returns an iterable of values in the map ..values(): IterableIterator<v>;
  console.log(users.entries())  //  Returns an iterable of key, value pairs for every entry in the map... entries(): IterableIterator<[K, V]>;


 for(const key of users.keys()){
   console.log(key)  
 }

 for(const value of users.values()){
  console.log(value)  
}
for (const [key,value] of users.entries()) {
  //console.log(key + " = " + value.fName +" "+value.lName)
  console.log( ` Key :${key} & Full Name: ${value.fName} ${value.lName}`)  // In string literal format
}

//ES5
 users.forEach(function (value,key){
  console.log( key + " = " + value.fName +" "+value.lName )
})

//ES6
//users.forEach((value,key)=>console.log(key + " = " + value.fName +" "+value.lName))
users.forEach( (value,key) => console.log( ` Key :${key} & Full Name: ${value.fName} ${value.lName}`))  // In string literal format


  